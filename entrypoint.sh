#!/bin/bash

if [ -z "$GLOBUS_CLIENT_ID" ]
then
    echo "Missing environment variable 'GLOBUS_CLIENT_ID'. Exiting."
    exit 1
fi

if [ -z "$GLOBUS_CLIENT_SECRET" ]
then
    echo "Missing environment variable 'GLOBUS_CLIENT_SECRET'. Exiting."
    exit 1
fi

if [ -z "$DEPLOYMENT_KEY" ]
then
    echo "Missing environment variable 'DEPLOYMENT_KEY'. Exiting."
    exit 1
fi

deployment_key="/deployment-key.json"
echo $DEPLOYMENT_KEY > $deployment_key
chmod 600 $deployment_key

#
# GCS will check for the existence of this file to make sure this is
# a supported platform.
#
if [ ! -f /bin/systemctl ]
then
    ln -s /bin/true /bin/systemctl
fi

#
# Setup a fake systemctl hook for relative-path callouts in start scripts.
#
if [ ! -e /systemctl ]
then
    ln -s /bin/true /systemctl
fi

PATH=/:$PATH globus-connect-server node setup --client-id $GLOBUS_CLIENT_ID --deployment-key $deployment_key $NODE_SETUP_ARGS $NODE_SETUP_RUNTIME_ARGS
if [ $? -ne 0 ]
then
    echo "Node setup failed. Exiting."
    exit 1
fi

#
# Launch Services
#
echo 'Launching rsyslogd'
/usr/sbin/rsyslogd

while [ ! -f /var/run/syslogd.pid ]
do
    sleep 0.5
done
rsyslogd_pid=`cat /var/run/syslogd.pid`


echo 'Launching sssd'
[ -d /var/log/sssd ] || mkdir -p /var/log/sssd
/usr/sbin/sssd

while [ ! -f /var/run/sssd.pid ]
do
    sleep 0.5
done
sssd_pid=`cat /var/run/sssd.pid`


echo 'Launching Apache httpd'
[ -d /var/log/httpd ] || mkdir -p /var/log/httpd
/sbin/httpd -k start

while [ ! -f /var/run/httpd/httpd.pid ]
do
    sleep 0.5
done
apache_httpd_pid=`cat /var/run/httpd/httpd.pid`


echo 'Launching GCS Manager'
cd /opt/globus/share/web
/opt/globus/bin/gunicorn --bind unix:/var/run/gcs_manager.sock \
    --user gcsweb --group gcsweb -D --pid /var/run/gunicorn.pid \
    --workers 4 --preload api_app

while [ ! -f /var/run/gunicorn.pid ]
do
    sleep 0.5
done
gcs_manager_pid=`cat /var/run/gunicorn.pid`
cd /


echo 'Launching GridFTP Server'
sudo -u gcsweb -g gcsweb touch /var/lib/globus-connect-server/gcs-manager/gridftp-key
/usr/sbin/globus-gridftp-server \
    -S \
    -c /etc/gridftp.conf \
    -C /etc/gridftp.d \
    -pidfile /run/globus-gridftp-server.pid

while [ ! -f /run/globus-gridftp-server.pid ]
do
    sleep 0.5
done
gridftp_pid=`cat /run/globus-gridftp-server.pid`


echo 'Launching GCS Assistant'
sudo -u gcsweb -g gcsweb /opt/globus/bin/globus-connect-server assistant &

function wait_on_pid()
{
    while :
    do
        kill -0 $1 > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            break
        fi
    done
}

function wait_on_process()
{
    while :
    do
        killall -0 $1 > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            break
        fi
    done
}

shutting_down=0
function cleanup()
{
    echo "Shutting down..."
    shutting_down=1
    trap - TERM
    trap - EXIT

    echo 'Terminating Apache httpd'
    /sbin/httpd -k stop

    echo 'Terminating GCS Manager'
    kill $gcs_manager_pid

    echo 'Terminating GCS Assistant'
    killall -TERM 'globus-connect-server assistant'

    echo Waiting on GCS Assistant
    wait_on_process 'globus-connect-server assistant'

    echo Waiting on GCS Manager
    wait_on_pid $gcs_manager_pid

    echo "Running node cleanup ..."
    PATH=/:$PATH globus-connect-server node cleanup
}

trap cleanup TERM
trap cleanup EXIT
trap '' HUP
trap '' INT

echo "GCS container successfully deployed"
while [ $shutting_down -eq 0 ]
do
    wait
done
