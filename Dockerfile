FROM centos:7

ENV LC_ALL=en_US.utf-8

COPY root/etc/rsyslog.conf /etc/rsyslog.conf
COPY root/etc/sssd.conf /etc/sssd/sssd.conf
COPY entrypoint.sh /entrypoint.sh

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
    yum install -y sssd rsyslog authconfig sudo && \
    yum install -y http://downloads.globus.org/toolkit/gt6/stable/installers/repo/rpm/globus-toolkit-repo-latest.noarch.rpm && \
    yum-config-manager --enable Globus-Connect-Server-5-Stable && \
    yum-config-manager --enable Globus-Toolkit-6-Stable && \
    yum clean all && \
    yum -y update && \
    yum install -y globus-connect-server54 && \
    chmod 0600 /etc/sssd/sssd.conf && \
    authconfig --enablesssdauth --enablesssd --update && \
    mkdir -p --mode=0755 /storage

# These are the default ports in use by GCSv5.4. Currently, they can not be changed.
#   443 : HTTPD service for GCS Manager API and HTTPS access to collections
#  50000-51000 : Default port range for incoming data transfer tasks
EXPOSE 443/tcp 50000-51000/tcp

# Default command unless overriden with 'docker run --entrypoint'
ENTRYPOINT ["/entrypoint.sh"]
# Default options to ENTRYPOINT unless overriden with 'docker run arg1...'
CMD []
