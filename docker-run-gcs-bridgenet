#!/bin/bash

usage() {
    echo "Usage: `basename $0` [OPTIONS]"
    echo
    echo "Runs the Globus Connect Server in Docker's bridge networking mode"
    echo
    echo "Options:"
    echo "GCS Node Setup Options"
    echo "  --incoming-port-range LOW_PORT HIGH_PORT"
    echo "                                 Allowed port range for incoming TCP data"
    echo "                                 connections  [default: 50000, 50100]"
    echo "                                 Also sets the published ports for the Docker container"
    echo "  --outgoing-port-range LOW_PORT HIGH_PORT"
    echo "                                 Port range used as the source for outgoing"
    echo "                                 TCP data connections"
    echo "  -i, --ip-address TEXT          IP address of the GCS Node. Use this option"
    echo "                                 multiple times to set multiple IPs"
    echo "  --data-interface TEXT          IP interface of the GCS Node used for Globus"
    echo "                                 data transfers"
    echo "  -v, --verbose                  Show additional output during node setup"
    echo
    echo "Docker Options"
    echo "  --docker-name NAME             Name to use for Docker container"
    echo "                                 [default: globus-connect-server]"
    echo "  --docker-network NAME          Name to use for Docker bridge network."
    echo "                                 Will be created if it does not exist."
    echo "                                 [default: globus-connect-server]"
    echo "  --env-file FILE                Environment file to pass to Docker"
    echo "                                 [default: .env_gcs]"
    echo "  --log-dir DIRECTORY            Directory to store log files from container"
    echo "                                 [default: ./var/log]"
    echo "  --volume HOST_DIR:CONTAINER_DIR:OPTIONS"
    echo "                                 Mount a volume. Use this option multiple"
    echo "                                 times to mount multiple volumes."
    echo "  --mount DOCKER_MOUNT_SYNTAX"
    echo "                                 Mount a volume. Use this option multiple"
    echo "                                 times to mount multiple volumes. Offers more"
    echo "                                 options than the --volume flag."
    echo "  -h, --help                     Show this message and exit."
}

#
# Usage: <cnt> <value1> <value2> ...
# cnt - Number of option values to expect
# valueN - remaining command line values
#
verify_option_value()
{
    option=$1
    expected_value_cnt=$2
    values=("${@:3:$(($expected_value_cnt))}")

    if [ ${#values[@]} -ne $expected_value_cnt ]
    then
        echo "Missing value for $option"
        exit 1
    fi

    for v in "${values[@]}"
    do
        if [[ ${v} == -* ]]
        then
            echo "Missing value for $option"
            exit 1
        fi
    done
}

ENV_FILE=".env_gcs"
LOG_DIR="$(pwd)/var/log"
DOCKER_NAME="globus-connect-server"
DOCKER_NETWORK="globus-connect-server"
DOCKER_RUN_ARGS=""
NODE_SETUP_RUNTIME_ARGS=""
INCOMING_LOW_PORT=50000
INCOMING_HIGH_PORT=50100
INCOMING_PORT_RANGE_SET_FLAG=0

while (( "$#" ))
do
  case $1 in
    --incoming-port-range)
      verify_option_value $1 2 "${@:2}"
      NODE_SETUP_RUNTIME_ARGS="$NODE_SETUP_RUNTIME_ARGS ${@:1:3}"
      INCOMING_LOW_PORT=${@:2:1}
      INCOMING_HIGH_PORT=${@:3:1}
      INCOMING_PORT_RANGE_SET_FLAG=1
      shift; shift; shift
      ;;
    --outgoing-port-range)
      verify_option_value $1 2 "${@:2}"
      NODE_SETUP_RUNTIME_ARGS="$NODE_SETUP_RUNTIME_ARGS ${@:1:3}"
      shift; shift; shift
      ;;
    -i | --ip-address)
      verify_option_value $1 1 "${@:2}"
      NODE_SETUP_RUNTIME_ARGS="$NODE_SETUP_RUNTIME_ARGS ${@:1:2}"
      shift; shift
      ;;
    --data-interface)
      NODE_SETUP_RUNTIME_ARGS="$NODE_SETUP_RUNTIME_ARGS ${@:1:2}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    -v | --verbose)
      NODE_SETUP_RUNTIME_ARGS="$NODE_SETUP_RUNTIME_ARGS $1"
      shift
      ;;
    --docker-name)
      DOCKER_NAME="${@:2:1}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    --docker-network)
      DOCKER_NETWORK="${@:2:1}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    --env-file)
      ENV_FILE="${@:2:1}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    --log-dir)
      LOG_DIR="${@:2:1}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    --volume)
      DOCKER_RUN_ARGS="$DOCKER_RUN_ARGS ${@:1:2}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    --mount)
      DOCKER_RUN_ARGS="$DOCKER_RUN_ARGS ${@:1:2}"
      verify_option_value $1 1 "${@:2}"
      shift; shift
      ;;
    -h | --help)
      usage
      exit 1
      ;;
    -*)
      echo "Unknown option $1"
      usage
      exit 1
      ;;
    *)
      echo "Unknown argument $1"
      usage
      exit 1
      ;;
  esac
done

if [ $INCOMING_PORT_RANGE_SET_FLAG -eq 0 ]
then
    NODE_SETUP_RUNTIME_ARGS="$NODE_SETUP_RUNTIME_ARGS --incoming-port-range $INCOMING_LOW_PORT $INCOMING_HIGH_PORT"
fi

tmpfile=$(mktemp /tmp/docker-run-gcs.envXXXXXX)
echo "NODE_SETUP_RUNTIME_ARGS=${NODE_SETUP_RUNTIME_ARGS}" > $tmpfile

if [[ ! $(docker network ls | grep "${DOCKER_NETWORK}") ]]
then
    docker network create "${DOCKER_NETWORK}"
fi

docker run \
    --name "${DOCKER_NAME}" \
    --restart unless-stopped \
    --stop-timeout 60 \
    --detach \
    --network "${DOCKER_NETWORK}" \
    -p 443:443 \
    -p ${INCOMING_LOW_PORT}-${INCOMING_HIGH_PORT}:${INCOMING_LOW_PORT}-${INCOMING_HIGH_PORT} \
    --env-file $tmpfile \
    --env-file "${ENV_FILE}" \
    --volume "${LOG_DIR}":/var/log:rw \
    $DOCKER_RUN_ARGS \
    globus-connect-server54:latest

rm $tmpfile
